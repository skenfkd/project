import axios from "axios";

const instance = axios.create({
  baseURL: "/", //请求路径
  timeout: 5000, //请求超时
});

instance.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    console.log(config);
    //设置请求头,给请求添加请求头添加token令牌
    config.headers["token"] = sessionStorage.getItem("token");
    return config;
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  }
);

instance.interceptors.response.use(
  function (response) {
    // 对响应数据做点什么
    let code = response.errno;
    return response;
    //登录状态token过期或者未登录
    if (parseInt(code) === 401 || parseInt(code) === 202) {
      //session无效
      Toast(response.data.msg);
      //跳转到登录页面
      setTimeout(() => {
        router.push({
          name: "login",
        });
      }, 1000);
      Promise.reject(response);
    } else if (parseInt(code) === 201) {
      Toast(response.data.msg);
      router.push({
        name: "Login",
      });
      Promise.reject(response);
    }

    if (response.data.msg == "ok") {
      return response;
    }
  },
  function (error) {
    // 对响应错误做点什么
    if (error.response) {
      switch (
        error.response.status //404，网络错误，地址有问题。
      ) {
        case 404:
          Toast("访问地址错误");
          break;
        case 401:
          Toast("未登录或登录已失效");
          setTimeout(() => {
            router.push({
              name: "login",
            });
          }, 1000);
          break;
      }
    } else {
      let errorMsg = error.message;
      if (error.message.indexOf("timeout") > -1) {
        errorMsg = "请求超时";
      }
      return Promise.resolve({
        //在报错的时候传回一个正常的promise对象，错误可以交到正常处理
        data: {
          code: 1,
          msg: error.message,
          extmsg: errorMsg,
        },
      });
    }
  }
);

export default instance;
