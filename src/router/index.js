import Vue from "vue";
import VueRouter from "vue-router";
import Login from "../components/Login/Login.vue";
import Home from "../components/Home/Home.vue";
import AuthorityLists from "../components/AuthorityLists/AuthorityLists.vue";
// import MyAuthorityList from "../components/AuthorityLists/MyAuthList.vue";
import AddAuthority from "../components/AddAuthority/AddAuthority.vue";
import AddTask from "../components/AddTask/AddTask.vue";
import AuthorityDetail from "../components/AuthorityDetail/AuthorityDetail.vue";
import AuthorityEdit from "../components/AuthorityDetail/AuthorityEdit.vue";

Vue.use(VueRouter);

const routes = [
	{
		// 默认不用登陆，如果cookie不存在，则强制返回
		path: "/",
		redirect: "/home",
		// meta: {
		//   showNav: false,
		// },
	},
	{
		path: "/login",
		component: Login,
	},
	// {
	//   path: "/home/authoritylist",
	//   name: "authoritylist",
	//   component: AuthorityList,
	//   // meta: {
	//   //   showNav: true,
	//   // },
	// },
	{
		path: "/login",
		component: Login,
	},
	{
		path: "/home",
		redirect: "/home/authoritylists",
	},
	{
    path: "/home",
		component: Home,
		children: [
			{
				path: "/home/AuthorityLists",
				name: "AuthorityLists",
				component: AuthorityLists,
				// meta: { keepAlive: true },
				// children: [
				//   {
				//     path: "/home/AuthorityDetail",
				//     name: "AuthorityDetail",
				//     component: AuthorityDetail
				//   }
				// ]
			},
			{
				path: "/home/addauthority",
				name: "addauthority",
				component: AddAuthority,
			},
			{
				path: "/home/addtask",
				name: "addtask",
				component: AddTask,
			},
			{
				path: "/home/AuthorityDetail",
				name: "AuthorityDetail",
				component: AuthorityDetail,
			},
			{
				path: "/home/AuthorityEdit",
				name: "AuthorityEdit",
				component: AuthorityEdit,
			},
			,
		],
	},
];

const router = new VueRouter({
	routes,
});

// 挂载路由导航守卫
router.beforeEach((to, from, next) => {
	// to 将要访问的路径 from 从哪个路径跳转而来
	// next 是一个函数，表示放行 next() 放行 next('/login') 强制跳转
	if (to.path === "/login") return next();
	// // 判断cookie是否存在，存在免登陆
	if (document.cookie.indexOf("webim:sess") === -1) {
		// Message({ message: "登录超时,请重新登录!", type: "info" });
		// return next("/login");
	}
	next();
});

export default router;
