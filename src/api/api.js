import http from "@/utils/interceptor.js";

// url基地址
let host = "/api/admin/role/";

// --------------模板------------------
// 主页-授权列表页
// 获取主页信息-默认我的授权
export const getAuthorizelist = (params) => {
  return http.get(`${host}/api/authorizelist/`, { params: params });
};

// 获取我的授权详细信息
export const getAuthorizedetailById = (params) => {
  return http.get(`${host}/api/authorizedetail/`, { params: params });
};

// 获取用户信息
export const getUserById = (params) => {
  return http.get(`${host}/api/user/`, { params: params });
};

// 获取主页信息-默认我的被授权
export const getAuthorizedlist = (params) => {
  return http.get(`${host}/api/authorizedlist/`, { params: params });
};

// 获取被授权详细信息
export const getAuthorizeddetail = (params) => {
  return http.get(`${host}/api/authorizeddetail/`, { params: params });
};

// 获取自定义任务列表
export const getCustomlist = (params) => {
  return http.get(`${host}/api/customlist/`, { params: params });
};

// 登录页
// 用户登录
export const postLogin = (params) => {
  return http.get(`${host}/api/login/`, { params: params });
};

// 新增转授权页
// 新增OA转授权
export const newOAauthority = (params) => {
  return http.get(`${host}/api/OAauthority/`, { params: params });
};

// 暂存OA转授权
export const OAauthoritystagging = (params) => {
  return http.get(`${host}/api/OAauthoritystagging/`, { params: params });
};

// 新增自定义转授权页
// 新增自定义任务
export const newCustomtask = (params) => {
  return http.get(`${host}/api/customtask/`, { params: params });
};

// 新增自定义转授权
export const newCustomauthority = (params) => {
  return http.get(`${host}/api/Customauthority/`, { params: params });
};

// 自定义任务编辑页
// 编辑自定义任务
export const editcustomtask = (params) => {
  return http.get(`${host}/api/editcustomtask/`, { params: params });
};

// AI推荐页
// 快速生成推荐
export const getAIrecommendlist = (params) => {
  return http.get(`${host}/api/AIrecommendlist/`, { params: params });
};

// 新增AI推荐授权
export const newAIrecommend = (params) => {
  return http.get(`${host}/api/AIrecommend/`, { params: params });
};

// 调用--在页面调用函数名就可以了
// 下面这个网页有详细信息
// https://www.jianshu.com/p/f6fd6fd73000
