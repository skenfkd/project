import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/login.vue'
import Index from '../components/index.vue'
import AddAuthority from '../components/addAuthority.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: {
        showNav: false,
      },
    },
    {
      path: '/index',
      name: 'index',
      component: Index,
      meta: {
        showNav: true,
      },
    },
    {
      path: '/addAuthority',
      name: 'addAuthority',
      component: AddAuthority,
      meta: {
        showNav: true,
      },
    },
  ]
})