var treeData = [
  {
    id: "001",
    label: "上海研发部上海开发三部",
    status: "未授权",
    isLeaf: false,
    children: [
      {
        id: "002",
        label: "会议管理系统",
        status: "未授权",
        isLeaf: false,
        children: [
          {
            id: "018",
            label: "普通用户",
            status: "已授权",
            isLeaf: true,
          },
        ],
      },
      {
        id: "003",
        label: "保密业务管理",
        status: "未授权",
        isLeaf: false,
        children: [
          {
            id: "019",
            label: "移动介质涉密载体普通人员",
            status: "已授权",
            isLeaf: true,
          },
          {
            id: "020",
            label: "经理（三级）以下",
            status: "已授权",
            isLeaf: true,
          },
        ],
      },
      {
        id: "004",
        label: "全球安全风险管理系统",
        status: "未授权",
        isLeaf: false,
        children: [
          {
            id: "021",
            label: "普通用户",
            status: "已授权",
            isLeaf: true,
          },
        ],
      },
      {
        id: "005",
        label: "公文系统",
        status: "未授权",
        isLeaf: false,
        children: [
          {
            id: "022",
            label: "普通用户",
            status: "已授权",
            isLeaf: true,
          },
        ],
      },
      {
        id: "006",
        label: "办公信息化系统",
        status: "未授权",
        isLeaf: false,
        children: [
          {
            id: "023",
            label: "帮助文档查看",
            status: "已授权",
            isLeaf: true,
          },
          {
            id: "024",
            label: "政府文件查看",
            status: "已授权",
            isLeaf: true,
          },
          {
            id: "025",
            label: "紧急公文查看",
            status: "已授权",
            isLeaf: true,
          },
          {
            id: "026",
            label: "行内公文查看",
            status: "已授权",
            isLeaf: true,
          },
          {
            id: "027",
            label: "规章制度查看",
            status: "已授权",
            isLeaf: true,
          },
        ],
      },
      {
        id: "007",
        label: "合同管理",
        status: "未授权",
        isLeaf: false,
        children: [
          {
            id: "028",
            label: "合同查阅人",
            status: "已授权",
            isLeaf: true,
          },
        ],
      },
      {
        id: "008",
        label: "因私出境系统",
        status: "未授权",
        isLeaf: false,
        children: [
          {
            id: "029",
            label: "普通用户",
            status: "已授权",
            isLeaf: true,
          },
        ],
      },
      {
        id: "009",
        label: "声誉风险管理系统",
        status: "未授权",
        isLeaf: false,
        children: [
          {
            id: "030",
            label: "普通用户",
            status: "已授权",
            isLeaf: true,
          },
        ],
      },
      {
        id: "010",
        label: "外部咨询项目信息安全管理系统",
        status: "未授权",
        isLeaf: false,
        children: [
          {
            id: "031",
            label: "经办人",
            status: "已授权",
            isLeaf: true,
          },
        ],
      },
      {
        id: "011",
        label: "对外提供客户信息登记管理系统",
        status: "未授权",
        isLeaf: false,
        children: [
          {
            id: "032",
            label: "经办人",
            status: "已授权",
            isLeaf: true,
          },
        ],
      },
      {
        id: "012",
        label: "日程系统",
        status: "未授权",
        isLeaf: false,
        children: [
          {
            id: "033",
            label: "普通用户",
            status: "已授权",
            isLeaf: true,
          },
        ],
      },
      {
        id: "013",
        label: "法律咨询审查系统",
        status: "未授权",
        isLeaf: false,
        children: [
          {
            id: "034",
            label: "送审发起人",
            status: "已授权",
            isLeaf: true,
          },
        ],
      },
      {
        id: "014",
        label: "涉密载体跟踪管理系统",
        status: "未授权",
        isLeaf: false,
        children: [
          {
            id: "035",
            label: "涉密载体普通人员",
            status: "已授权",
            isLeaf: true,
          },
        ],
      },
      {
        id: "015",
        label: "督办管理系统",
        status: "未授权",
        isLeaf: false,
        children: [
          {
            id: "036",
            label: "普通用户",
            status: "已授权",
            isLeaf: true,
          },
        ],
      },
      {
        id: "016",
        label: "移动介质数据外发审批系统",
        status: "未授权",
        isLeaf: false,
        children: [
          {
            id: "037",
            label: "移动介质数据外发审批系统普通用户",
            status: "已授权",
            isLeaf: true,
          },
        ],
      },
      {
        id: "017",
        label: "行政印章系统",
        status: "未授权",
        isLeaf: false,
        children: [
          {
            id: "038",
            label: "普通用户",
            status: "已授权",
            isLeaf: true,
          },
        ],
      },
    ],
  },
];
var checkedAuthList = ["002", "018", "017", "038"];

var cardDataList = [
  {
    id: "001",
    taskName: "用户休假转授权",
    detail: "休假时暂时转移权限",
    checkedAuthList: ["002", "018", "017", "038"],
  },
  {
    id: "002",
    taskName: "用户离职转授权",
    detail: "离职时永久转移权限",
    checkedAuthList: ["003", "019", "020", "004","021"],
  },
  {
    id: "003",
    taskName: "用户退休转授权",
    detail: "退休时永久转移权限",
    checkedAuthList: ["005", "022", "006", "023"],
  },
  {
    id: "004",
    taskName: "用户升职转授权",
    detail: "用户升职转移权限",
    checkedAuthList: ["007", "028", "008", "029"],
  },
  {
    id: "005",
    taskName: "会议转授权",
    detail: "转移会议权限",
    checkedAuthList: ["006", "024", "009", "030"],
  },
  {
    id: "006",
    taskName: "通用项目转授权",
    detail: "普通项目权限转移",
    checkedAuthList: ["002", "018", "017", "038"],
  },
  {
    id: "000",
    taskName: "新增授权",
    detail: "",
  },
];

var taskData = {
  id: "001",
  taskName: "用户休假转授权",
  detail: ["休假时暂时转移权限"],
};
