# 工程注意事项

# assets 使用

images 用来存放使用的图片

fonts 用来引入想要使用的字体

iconfont 用来引入图标 阿里图标库

css 里面用来写一些全局 css 样式

# element ui 组件使用

引入时请去 \src\plugins\element.js 查找有无，没有按照以下格式引入后使用

import { 组件名} from "element-ui";
Vue.use(组件名);

# git 提交注意

git commit -m "修改部分描述"

建议建立自己的分支，提交前先拉取远程，合并完冲突后，再合并到 master

# project1

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run your tests

```
npm run test
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
